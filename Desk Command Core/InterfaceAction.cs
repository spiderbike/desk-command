﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Desk_Command_Core
{
    public interface InterfaceAction
    {
        void Do();
    }
}
